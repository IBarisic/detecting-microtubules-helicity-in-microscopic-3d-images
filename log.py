import skimage.filters as skf
import scipy
gf1d=scipy.ndimage.filters.gaussian_filter1d

def Gauss3D(I, sigma):
  im_gauss = gf1d(I, sigma[0], 0)
  im_gauss = gf1d(im_gauss, sigma[1], 1)
  im_gauss = gf1d(im_gauss, sigma[2], 2)
  return im_gauss


def laplace(I):
  I2 = I.copy()
  for i in range(I.shape[2]):
    I2[:,:,i] = skf.laplace(I[:,:,i])
  return I2


def LoG(I,sigma):
  return laplace(Gauss3D(I,sigma))