This is the source code of my bachelor's thesis "Detecting microtubules' helicity in microscopic 3D images".

The code in the file "log.py" and part of the code in the file "flow.py" has been written by my mentor professor Siniša Šegvić.

Images (slices) of each cell should be stored in a folder named as the number of that cell ("1", "2", "3"...). Images of each cell should be named sequentially starting from 0: "i000.png", "i001.png", ... Notice that names use 3 digits and the images are "png". "i000.png" should be the bottom slice of the cell, "i001.png" the slice above it...

Images should be 8-bit.

File with the coordinates of the poles of each cell should be stored in a folder named as the number of that cell ("1", "2", "3"...). File should be named "polovi.csv".

File "myio.py" contains the code responsible for loading the images of the selected cell.
The only thing that should be changed in this file is the content of the variable parent_folder. It should contain the path of the folder that contains the subfolders with the images of each cell ("1", "2", "3"...).

File "log.py" contains functions responsible for calculating the LoG filter (Laplacian of Gaussian).
Nothing should be changed in this file.

File "flow.py" contains the code that loads the images and applies the LoG filter to each image using the functions from "myio.py" and "log.py". After applying the LoG filter it calculates the optical flow between each two consecutive slices of the image.
Things that can be changed in this file:
1) you can choose to sample every 6th column in the images, apply the LoG filter to the sampled images and then restore the image size or you can apply the LoG filter to the original images (without previously sampling). The first option is currently chosen (with sampling), if you want to use the second option (without sampling) you need to set "I2" to be equal to "I" (remove sampling), comment the line that calls the "restore_size" function and also in the "LoG" function you should choose the line with the appropriate sigma (read the comments in the function).
2) you can save images after applying the LoG filter by uncommenting the line that calls the "save_images" function. You can save images with the color-coded optical flow by uncommenting the line that calls the "save_flow" function.

File "helicity.py" contains the main program which calculates the helicity of each cell using five different methods and prints those helicities as well as the average helicity of each method.
Things that can be changed in this file:
1) the content of the variable "parent_folder" in the "poles_load" function. It should contain the path of the folder that contains the subfolders ("1", "2", "3"...) with the poles' coordinates files ("polovi.csv").
2) numbers of the first and the last cell you want to calculate the helicity for (variables "start" and "end"). If you want to calculate the helicity of only one cell then set the "start" and "end" variables to the same value.
3) uncommenting the lines that call the visualization methods if you wish to use them (grid_displacements and distribution).

File "blobs_test.py" draws manual labels over computed blobs to check if they match.
Things that can be changed in this file:
1) content of the variable "parent_folder" in the function "load" (already described above in the section with the file "myio.py").
2) content of the variable "parent_folder" in the function "points_load". It should contain the path of the folder that contains the subfolders ("1", "2", "3"...) with the csv files with the manual labels of the microtubules. The csv files should be named "plot3d.csv".
3) (not) sampling every 6th column (already described above in the point 1 of the section with the file "flow.py").