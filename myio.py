import skimage.io as skio
import numpy as np
import os


def load(cell_num):
  parent_folder = "C:\\Users\\Ivan\\Desktop\\faks\\6. semestar\\Završni rad\\Ruđer\\centrirane stanice\\razdvojene"
  images = os.listdir( parent_folder + '\\{:01d}\\'.format(cell_num))
  z = len(images) - 2 #-1 because indices start from 0, not from 1, and -1 because of hidden file 'Thumbs.db' on Windows

  #find out dimensons
  name = 'i000.png'
  im = skio.imread(parent_folder + '\\{:01d}\\'.format(cell_num) + name)

  h = im.shape[0]
  w = im.shape[1]

  I = np.zeros((h, w, z + 1))

  for i in range(z+1):
    name = 'i' + '{:03d}'.format(i) + '.png'
    im = skio.imread(parent_folder + '\\{:01d}\\'.format(cell_num) + name)
    I[:, :, i] = im
  return I