import numpy as np
import cv2
import skimage.io as skio
import matplotlib.pyplot as plt
import skimage.transform as skt

def float_to_uint8(I):
    I255 = I * 255 / np.max(I)
    Iuint8 = I255.astype(np.uint8)
    return Iuint8


def uint8_to_rgb24(I):
    Irgb = np.tile(I.reshape(I.shape + (1,)), (1, 1, 3))
    return Irgb


def threshold_rel(I, th):
    Ith = I.copy()
    Ith[Ith < th * np.max(I)] = 0
    return Ith


def flow(I):
    I2 = np.zeros((I.shape[0], I.shape[1], 2, I.shape[2] - 1))
    for i in range(I.shape[2] - 1):
        I0, I1 = I[:, :, i], I[:, :, i + 1]
        I2[:, :, :, i] = cv2.calcOpticalFlowFarneback(
            I0, I1, None, 0.5, 3, 15, 3, 10, 1.2, 0)
    return I2


def add_legend(legend, n):
    m = n
    legend[0 * n:1 * n, 0 * m:1 * m, :] = np.array([-1, -1])
    legend[0 * n:1 * n, 1 * m:2 * m, :] = np.array([0, -1])
    legend[0 * n:1 * n, 2 * m:3 * m, :] = np.array([1, -1])
    legend[1 * n:2 * n, 0 * m:1 * m, :] = np.array([-1, 0])
    legend[1 * n:2 * n, 2 * m:3 * m, :] = np.array([1, 0])
    legend[2 * n:3 * n, 0 * m:1 * m, :] = np.array([-1, 1])
    legend[2 * n:3 * n, 1 * m:2 * m, :] = np.array([0, 1])
    legend[2 * n:3 * n, 2 * m:3 * m, :] = np.array([1, 1])
    return legend


def draw_hsv2(flow):
    """Color codes optical flow for visualization.
        :param flow: one slice of the output of the flow() function
    """
    h, w = flow.shape[:2]
    mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1])
    hsv = np.zeros((h, w, 3), np.uint8)
    hsv[..., 0] = ang * (180 / np.pi / 2)
    hsv[..., 1] = 255
    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)
    return rgb


def LoG(I, sigma):
    import log
    #Il = log.LoG(I, [sigma, 1.7*sigma, sigma]) #used when there IS NO sampling of every 6th column
    Il = log.LoG(I, [sigma, sigma / 2, sigma]) #used when there IS sampling of every 6th column
    return Il


def restore_size(Il, I):
    imgs = np.zeros(I.shape)
    for i in range(Il.shape[2]):
        Ils = skt.resize(Il[:, :, i], I[:, :, i].shape)
        imgs[:, :, i] = Ils

    return imgs


def save_images(I, cell_num):
    for i in range(I.shape[2]):
        skio.imsave("C:\\Users\\Ivan\\Desktop\\zagladene\\" + "{:01d}".format(cell_num) + "\\i" + "{:03d}".format(i) + ".png", float_to_uint8(I[:,:,i]))


def save_flow(flow, Il, cell_num):
    d_l = 10
    legend = add_legend(np.zeros([3 * d_l, 3 * d_l, 2]), d_l)
    legend = draw_hsv2(legend)
    for i in range(flow.shape[3]):
        f = flow[:,:,:,i]
        f_vis = draw_hsv2(f)
        f_vis[0:3 * d_l, 0:3 * d_l, :] = legend

        mask = np.zeros(f.shape)
        mask[:, :, 0] = mask[:, :, 1] = Il[:,:,i]
        f_mask = f * mask
        f_mask_vis = draw_hsv2(f_mask)
        f_mask_vis[0:3 * d_l, 0:3 * d_l, :] = legend

        im = np.concatenate([
            uint8_to_rgb24(float_to_uint8(Il[:, :, i])), f_vis, f_mask_vis], axis=0)

        plt.imsave("C:\\Users\\Ivan\\Desktop\\flowpics\\" + "{:01d}".format(cell_num) + "\\i" + "{:03d}".format(i) + ".png", im)


def load_and_flow(cell_num):
    import myio
    I = myio.load(cell_num)
    I2=I[:,::6,:]
    #I = I[:,:,::5] #considers only every 5th slice of the cell

    Il = LoG(I2, 2)
    Il = threshold_rel(Il, 0.1)

    Il = restore_size(Il, I)

    #show one slice of the cell (for testing purposes)
    """
    plt.imshow(Il[:,:,63], cmap="gray")
    plt.title("Close the window to continue running the program.")
    plt.show()
    """

    #save_images(Il, cell_num)

    If = flow(float_to_uint8(Il))

    #show one slice of the flow (for testing purposes)
    """
    flow_vis = draw_hsv2(If[:,:,:,63])
    plt.imshow(flow_vis)
    plt.title("Close the window to continue running the program.")
    plt.show()
    """

    #save_flow(If, Il, cell_num)

    return Il, If