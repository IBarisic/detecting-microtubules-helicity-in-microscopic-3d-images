from skimage import measure
import matplotlib.pyplot as plt
import numpy as np
import flow
import csv
import warnings
import time
import math
import matplotlib.ticker as plticker
import matplotlib.patches as patches

img_pixel_size_x = 0.083
img_pixel_size_y = 0.080
img_dz_um = 0.083


def poles_load(i):
    parent_file = "C:\\Users\\Ivan\\Desktop\\faks\\6. semestar\\Završni rad\\Ruđer\\centrirane stanice\\ivan_fer"
    name = "polovi.csv"
    file = open(parent_file + '\\{:01d}\\'.format(i) + name)
    reader = csv.reader(file)

    poles = [np.zeros((2,)), np.zeros((2,))]
    j = 0
    for line in reader:
        if j != 0:  # skipping first line
            poles[j - 1][0] = line[1]
            poles[j - 1][1] = line[2]
        j += 1

    return poles

def connected_components(I, th):
    blobs = abs(I) > (th * np.max(abs(I)))
    blobs_labels, blobs_num = measure.label(blobs, return_num=True)
    return blobs_labels, blobs_num


def components_centers(labels, labels_num, z):
    """returns list of numpy arrays with the coordinates of the center of each labeled component
    """
    x_coordinates_sum = [0] * labels_num
    y_coordinates_sum = [0] * labels_num
    labels_occurence = [0] * labels_num
    comp_centers = [None] * labels_num

    for i in range(labels.shape[0]):
        for j in range(labels.shape[1]):
            if labels[i, j] != 0:
                # notice: "i" is y coordinate, "j" is x coordinate
                x_coordinates_sum[labels[i, j] - 1] += j
                y_coordinates_sum[labels[i, j] - 1] += i
                labels_occurence[labels[i, j] - 1] += 1

    for i in range(labels_num):
        x = x_coordinates_sum[i] / labels_occurence[i]
        y = y_coordinates_sum[i] / labels_occurence[i]
        comp_centers[i] = np.array([x, y, z])
    return comp_centers


def poles_conversion(lower_pole, upper_pole, I):
    """converts poles' coordinates from micrometers to pixels
    """
    min_z_pixel = 0
    max_z_pixel = I.shape[2] - 1

    x1_um = lower_pole[0]
    y1_um = lower_pole[1]
    x2_um = upper_pole[0]
    y2_um = upper_pole[1]

    x1_pixels = x1_um / img_pixel_size_x
    y1_pixels = y1_um / img_pixel_size_y
    z1_pixels = min_z_pixel

    x2_pixels = x2_um / img_pixel_size_x
    y2_pixels = y2_um / img_pixel_size_y
    z2_pixels = max_z_pixel

    return np.array([x1_pixels, y1_pixels, z1_pixels]), np.array([x2_pixels, y2_pixels,  z2_pixels])


def line_parameters(point1, point2):
    """returns parameters (direction vector and a point) of line that passes through given points
    """
    line_direction_vector = point2 - point1
    line_point = point1 #it doesn't matter if point1 or point2 is chosen
    return line_direction_vector, line_point


def central_axis_coordinates(z, axis_direction_vector, axis_point):
    """calculates x and y coordinates of the central axis of the cell for a given z coordinate based on 3D line equation through cell's poles
    """
    t = (z - axis_point[2])/axis_direction_vector[2] #derived from z == point[2] + direction_vector[2]*t
    x = axis_point[0] + axis_direction_vector[0] * t
    y = axis_point[1] + axis_direction_vector[1] * t
    return np.array([x, y])


def angle(point, central_axis_direction_vector, central_axis_point):
    """returns angle IN RADIANS
    """
    central_axis_coord_pixels = central_axis_coordinates(point[2], central_axis_direction_vector, central_axis_point)

    central_axis_x_um = central_axis_coord_pixels[0] * img_pixel_size_x
    central_axis_y_um = central_axis_coord_pixels[1] * img_pixel_size_y

    x = point[0] * img_pixel_size_x
    y = point[1] * img_pixel_size_y

    ang = math.atan2(y - central_axis_y_um, x - central_axis_x_um)

    return ang


def calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point):
    """calculates dphi IN DEGREES
    """
    moved_point = np.array([point[0] + dx, point[1] + dy, point[2]+1])
    phi1 = angle(point, central_axis_direction_vector, central_axis_point)
    phi2 = angle(moved_point, central_axis_direction_vector, central_axis_point)
    dphi = phi2 - phi1
    dphi = math.degrees(dphi)
    if dphi <= -180:
        dphi += 360
    if dphi > 180:
        dphi -= 360
    return dphi


def blob_center_helicity(I, I_flow, pole1, pole2, threshold):
    '''calculates helicity based on the following steps:
        1. find the center of each blob
        2. calculate helicity of each blob's center (based on optical flow between each two consecutive frames)
        3. calculate total helicity as average of helicities calculated in step 2
    '''
    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(pole1, pole2)
    dphi_sum = 0
    blobs_num = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)
        centers = components_centers(labels, labels_num, i)
        blobs_num += len(centers)

        for center in centers:
            # notice: first y, then x coordinate in flow (center[1], center[0]) because of internal representation of image
            dx = I_flow[int(round(center[1])), int(round(center[0])), 0, i]
            dy = I_flow[int(round(center[1])), int(round(center[0])), 1, i]
            dphi = calc_dphi(center, dx, dy, central_axis_direction_vector, central_axis_point)
            dphi_sum += dphi

    dphi_avg = dphi_sum / blobs_num
    helicity = dphi_avg / img_dz_um
    return helicity


def eight_environment_center_helicity(I, I_flow, pole1, pole2, threshold):
    '''Similar to blob_center_helicity, but instead of using only the central pixel of the blob,
        it uses the central pixel and its 8 surrounding pixels.'''
    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(pole1, pole2)
    dphi_sum = 0
    blobs_num = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)
        centers = components_centers(labels, labels_num, i)
        blobs_num += len(centers)

        for center in centers:
            dx_sum = 0
            dy_sum = 0
            for a in range(-1, 2):
                for b in range(-1, 2):
                    if ((int(round(center[1]))+a > I_flow.shape[0])\
                            or (int(round(center[1]))+a < 0)\
                            or (int(round(center[0]))+b > I_flow.shape[1])\
                            or (int(round(center[0]))+b < 0)):
                        #check if pixel is inside the image
                        continue
                    # notice: first y, then x coordinate in flow (center[1], center[0]) because of internal representation of image
                    dx = I_flow[int(round(center[1])) + a, int(round(center[0])) + b, 0, i]
                    dy = I_flow[int(round(center[1])) + a, int(round(center[0])) + b, 1, i]

                    dx_sum += dx
                    dy_sum += dy

            dx_avg = dx_sum / 9
            dy_avg = dy_sum / 9

            dphi = calc_dphi(center, dx_avg, dy_avg, central_axis_direction_vector, central_axis_point)
            dphi_sum += dphi

    dphi_avg = dphi_sum / blobs_num
    helicity = dphi_avg / img_dz_um
    return helicity


def whole_blob_helicity(I, I_flow, pole1, pole2, threshold):
    '''calculates helicity based on the following steps:
        1. find connected components in each image (find out which pixels belong to which blobs)
        2. find helicity for each pixel (based on optical flow between each two consecutive frames)
        3. calculate helicity for each blob as average helicity of all pixels that belong to a certain blob
        4. calculate total helicity as average of helicities calculated in step 3
    '''
    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(pole1, pole2)
    dphi_avg_sum = 0
    blobs_num = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)

        #print("number of connected components on current slice", labels_num)

        blobs_dphi = [0] * labels_num
        blobs_pixels_num = [0] * labels_num
        blobs_num += labels_num

        for j in range(I.shape[0]):
            for k in range(I.shape[1]):
                label = labels[j, k]
                if label == 0: continue
                dx = I_flow[j, k, 0, i]
                dy = I_flow[j, k, 1, i]
                # notice: x is k and y is j because of internal representation of image
                point = np.array([k, j, i])
                dphi = calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point)
                blobs_dphi[label - 1] += dphi
                blobs_pixels_num[label - 1] += 1

        for l in range(labels_num):
            dphi_avg_sum += (blobs_dphi[l]/blobs_pixels_num[l])

    dphi_avg = dphi_avg_sum / blobs_num
    helicity = dphi_avg / img_dz_um
    return helicity


def all_pixels_helicity(I, I_flow, lower_pole, upper_pole, threshold):
    '''calculates helicity based on the following steps:
        1. find helicity for each pixel (based on optical flow between two consecutive frames)
        2. calculate total helicity as average of helicities calculated in step 1
    '''
    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(lower_pole, upper_pole)
    dphi_sum = 0
    pixels_num = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)

        for j in range(I.shape[0]):
            for k in range(I.shape[1]):
                label = labels[j, k]
                if label == 0: continue
                dx = I_flow[j, k, 0, i]
                dy = I_flow[j, k, 1, i]
                # notice: x is k and y is j because of internal representation of image
                point = np.array([k, j, i])
                dphi_sum += calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point)
                pixels_num += 1

    dphi_avg = dphi_sum / pixels_num
    helicity = dphi_avg / img_dz_um
    return helicity


def all_pixels_weighted_helicity(I, I_flow, lower_pole, upper_pole, threshold):
    '''calculates helicity based on the following steps:
        1. find helicity for each pixel (based on optical flow between two consecutive frames)
        2. calculate total helicity as weighted average of helicities calculated in step 1 (weights are pixel intensities)
    '''
    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(lower_pole, upper_pole)
    dphi_sum = 0
    intensity_sum = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)

        for j in range(I.shape[0]):
            for k in range(I.shape[1]):
                label = labels[j, k]
                if label == 0: continue
                dx = I_flow[j, k, 0, i]
                dy = I_flow[j, k, 1, i]
                # notice: x is k and y is j because of internal representation of image
                point = np.array([k, j, i])
                dphi_sum += calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point)*I[j,k,i]
                intensity_sum += I[j,k,i]

    dphi_weighted = dphi_sum / intensity_sum
    helicity = dphi_weighted / img_dz_um

    return helicity


def grid_displacements(I, I_flow, threshold):
    """USED ONLY FOR VISUALIZATION.
        Draws a grid and an arrow for each rectangle in the grid.
        Arrow represents total displacement (optical flow) of all pixels in that rectangle.
        Draws arrows only for the 40% of the slices in the middle in the middle of the cell.
        Press left mouse button to move to the next slice.
        When you pass through all the slices in a cell the program with automatically close.

        WARNING: Do NOT close the window with the cell directly (pressing 'X' button), pass through all of the slices of
            the cell and whe window will close automatically, the program will stop working if you
            close the window directly.
        """
    slices_num = I.shape[2]
    img_height_pixels = I.shape[0]
    img_width_pixels = I.shape[1]

    bins_x = 6
    bins_y = 15
    bins = np.zeros((bins_y, bins_x, slices_num - 1, 2))
    bins_pixel_num = np.zeros((bins_y, bins_x, slices_num - 1))

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)
        for j in range(I.shape[0]):
            for k in range(I.shape[1]):
                label = labels[j, k]
                if label == 0: continue
                dx = I_flow[j, k, 0, i]
                dy = I_flow[j, k, 1, i]

                bins[math.floor(j/img_height_pixels*bins_y), math.floor(k/img_width_pixels*bins_x), i, 0] += dx
                bins[math.floor(j/img_height_pixels*bins_y), math.floor(k/img_width_pixels*bins_x), i, 1] += dy

                bins_pixel_num[math.floor(j / img_height_pixels * bins_y), math.floor(k / img_width_pixels * bins_x), i] += 1

    #normalization
    for i in range(slices_num - 1):
        for j in range(bins_y):
            for k in range(bins_x):
                if bins_pixel_num[j, k, i] != 0:
                    bins[j, k, i, 0] /= bins_pixel_num[j, k, i]
                    bins[j, k, i, 1] /= bins_pixel_num[j, k, i]

    aggregated_bins = np.zeros((bins_y, bins_x, math.floor((slices_num - 1)/5) + 1, 2))

    for i in range(slices_num - 1):
        for j in range(bins_y):
            for k in range(bins_x):
                aggregated_bins[j, k, math.floor(i / 5), 0] += bins[j, k, i, 0]
                aggregated_bins[j, k, math.floor(i / 5), 1] += bins[j, k, i, 1]

    img_res_x = I.shape[1]
    img_res_y = I.shape[0]

    bin_res_x = img_res_x / bins_x
    bin_res_y = img_res_y / bins_y
    
    my_dpi = 300.
    fig = plt.figure(figsize=(float(I.shape[0])/my_dpi, float(I.shape[1])/my_dpi), dpi=my_dpi)
    ax = fig.add_subplot(111)
    ax.set_title("Press the image to see the next one. Don't press x or the program will crash.")
    fig.subplots_adjust(left=0,right=1,bottom=0,top=1)
    my_interval_x = bin_res_x
    my_interval_y = bin_res_y
    loc_x = plticker.MultipleLocator(base=my_interval_x)
    loc_y = plticker.MultipleLocator(base=my_interval_y)

    offset_x = bin_res_x / 2
    offset_y = bin_res_y / 2

    plt.ion()
    for i in range(aggregated_bins.shape[2]):
        ax.xaxis.set_major_locator(loc_x)
        ax.yaxis.set_major_locator(loc_y)
        ax.grid(which="major", axis="both", linestyle="-", linewidth=0.1)
        ax.imshow(I[:, :, 5 * i], cmap="gray")
        for j in range(bins_y):
            for k in range(bins_x):
                arrow = patches.Arrow(k*bin_res_x + offset_x, j*bin_res_y + offset_y, aggregated_bins[j, k, i, 0], aggregated_bins[j, k, i, 1])
                ax.add_patch(arrow)

        plt.show()
        plt.waitforbuttonpress()
        ax.clear()

    #fig.savefig("C:\\Users\Ivan\\Desktop\\strelice test\\strelica.png", dpi=my_dpi)
    plt.close()


def each_pixels_helicities(I, I_flow, lower_pole, upper_pole, threshold):
    """Calculates and returns helicity of every pixel in the cell.
        Result of this function is used in distribution() function.
    """
    helicities = []

    slices_num = I.shape[2]

    central_axis_direction_vector, central_axis_point = line_parameters(lower_pole, upper_pole)
    dphi_sum = 0
    pixels_num = 0

    # considering only 40% of the images in the middle of the cell
    for i in range(int(0.3 * (slices_num - 1)), int(0.7 * (slices_num - 1))):
        labels, labels_num = connected_components(I[:, :, i], threshold)

        for j in range(I.shape[0]):
            for k in range(I.shape[1]):
                label = labels[j, k]
                if label == 0: continue
                dx = I_flow[j, k, 0, i]
                dy = I_flow[j, k, 1, i]
                # notice: x is k and y is j because of internal representation of image
                point = np.array([k, j, i])
                dphi_sum += calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point)

                helicities.append(calc_dphi(point, dx, dy, central_axis_direction_vector, central_axis_point)/img_dz_um)

                pixels_num += 1

    return helicities


def distribution(helicites, manual_mean = None, calculated_mean = None):
    """USED ONLY FOR VISUALIZATION.
        Draws distribution of each pixel's helicity.

        Parameters:
        manual_mean - helicity of the given cell calculated using handmade labels
                    - painted in green color if provided
        calculated_mean - helicity of the given cell calculated using automatical method
                        - painted in red color if provided

        WARNING: Do NOT close the window with the distribution directly (pressing 'X' button),
            press the image and the window will close automatically, the program will stop working if you
            close the window directly.
        """
    fig, axs = plt.subplots(1, 1)
    axs.hist(helicites, bins = 10000)
    axs.set_xlim(-10, 20)
    if calculated_mean is not None:
        plt.axvspan(calculated_mean - 0.02, calculated_mean + 0.02, color='red', alpha=0.5)
    if manual_mean is not None:
        plt.axvspan(manual_mean - 0.02, manual_mean + 0.02, color='green', alpha=0.5)
    plt.waitforbuttonpress()
    plt.close()


if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    center_sum = 0
    whole_sum = 0

    threshold = 0

    start = 1
    end = 40
    img_num = end - start + 1
    center_helicities = [None] * img_num
    eight_env_helicities = [None] * img_num
    whole_helicities = [None] * img_num
    all_pixels_helicities = [None] * img_num
    all_pixels_weighted_helicities = [None] * img_num

    start_time = time.time()
    for i in range(start, end + 1):
        I, I_flow = flow.load_and_flow(i)
        orig_lower_pole, orig_upper_pole = poles_load(i)
        lower_pole, upper_pole = poles_conversion(orig_lower_pole, orig_upper_pole, I)
        center_helicities[(i-start) - 1] = blob_center_helicity(I, I_flow, lower_pole, upper_pole, threshold)
        eight_env_helicities[(i - start) - 1] = eight_environment_center_helicity(I, I_flow, lower_pole, upper_pole, threshold)
        whole_helicities[(i-start) - 1] = whole_blob_helicity(I, I_flow, lower_pole, upper_pole, threshold)
        all_pixels_helicities[(i - start) - 1] = all_pixels_helicity(I, I_flow, lower_pole, upper_pole, threshold)
        all_pixels_weighted_helicities[(i - start) - 1] = all_pixels_weighted_helicity(I, I_flow, lower_pole, upper_pole, threshold)

        #grid_displacements(I, I_flow, threshold) #used for visualization, read function description

        #helicites = each_pixels_helicities(I, I_flow, lower_pole, upper_pole, threshold)
        #distribution(helicites) #used for visualization, read function description

        print(i)
        print("blob center helicity: ", center_helicities[(i-start) - 1])
        print("eight environment blob center helicity: ", eight_env_helicities[(i - start) - 1])
        print("whole blob helicity: ", whole_helicities[(i-start) - 1])
        print("all pixels helicity: ", all_pixels_helicities[(i - start) - 1])
        print("all pixels weighted helicity: ", all_pixels_weighted_helicities[(i - start) - 1])
        print()


    elapsed_time = time.time() - start_time

    center_avg_deg = np.average(center_helicities)
    eight_env_avg_deg = np.average(eight_env_helicities)
    whole_avg_deg = np.average(whole_helicities)
    all_pixels_avg_deg = np.average(all_pixels_helicities)
    all_pixels_weighted_avg_deg = np.average(all_pixels_weighted_helicities)

    center_dev_deg = np.std(center_helicities)
    eight_env_dev_deg = np.std(eight_env_helicities)
    whole_dev_deg = np.std(whole_helicities)
    all_pixels_dev_deg = np.std(all_pixels_helicities)
    all_pixels_weighted_dev_deg = np.std(all_pixels_weighted_helicities)

    print("average blob center helicity: ", center_avg_deg, "+-", center_dev_deg)
    print("average eight environment helicity: ", eight_env_avg_deg, "+-", eight_env_dev_deg)
    print("average whole blob helicity: ", whole_avg_deg, "+-", whole_dev_deg)
    print("average all pixels helicity: ", all_pixels_avg_deg, "+-", all_pixels_dev_deg)
    print("average all pixels weighted helicity: ", all_pixels_weighted_avg_deg, "+-", all_pixels_weighted_dev_deg)

    print("elapsed time: ", elapsed_time)
    print("average cell processing time: ", elapsed_time / (end - start + 1))