"""
Draws manual labels over computed blobs to check if they match.
"""
import numpy as np
import skimage.io as skio
import skimage.transform as skt
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import csv
from PIL import Image
import warnings


def threshold_rel(I, th):
    Ith = I.copy()
    Ith[Ith < th * np.max(I)] = 0
    return Ith


def LoG(I, sigma):
    import log
    Il = log.LoG(I, [sigma, sigma / 2, sigma]) #used when there IS sampling of every 6th column
    #Il = log.LoG(I, [sigma, 1.7*sigma, sigma]) #used when there IS NO sampling of every 6th column
    return Il


def points_load(cell):
    parent_file = "C:\\Users\\Ivan\\Desktop\\faks\\6. semestar\\Završni rad\\Ruđer\\Ivan"
    name = "plot3d.csv"
    file = open(parent_file + '\\{:01d}\\'.format(cell) + name)
    reader = csv.reader(file)

    """each key in slice_points dictionary is a number representing a slice,
        and value assigned to each key is a list of points (labels) on that slice"""
    slice_points = dict()

    flag = False
    for line in reader:
        if flag == True: #used for skipping the first line in the csv file (it contains irrelevant data)
            if len(line[0]) == 0: break

            point = []
            point.append(float(line[1]))
            point.append(float(line[2]))

            if int(line[3]) not in slice_points:
                slice_points[int(line[3])] = []
            slice_points[int(line[3])].append(point)

        flag = True

    return slice_points


def load(cell):
    import os
    parent_folder = "C:\\Users\\Ivan\\Desktop\\raspakirane"
    images = os.listdir(parent_folder + '\\{:01d}\\'.format(cell))

    z = len(
        images) - 2  # -1 because indices start from 0, and -1 because of hidden file 'Thumbs.db' (on Windows) or '.DS_Store' (on Mac)

    # find out dimensons
    name = 'i000.png'
    im = skio.imread(parent_folder + '\\{:01d}\\'.format(cell) + name)

    h = im.shape[0]
    w = im.shape[1]

    I = np.zeros((h, w, z + 1))

    for i in range(z + 1):
        name = 'i' + '{:03d}'.format(i) + '.png'
        im = np.array(Image.open(parent_folder + '\\{:01d}\\'.format(cell) + name), dtype=np.uint8)
        I[:, :, i] = im
    return I


def restore_size(Il, I):
    imgs = np.zeros(I.shape)
    for i in range(Il.shape[2]):
        Ils = skt.resize(Il[:, :, i], I[:, :, i].shape)
        imgs[:, :, i] = Ils

    return imgs


if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    cell = 3
    slices_points = points_load(cell)

    Iraw = load(cell)
    I = Iraw[:, ::6, :]
    #I = Iraw
    Il = LoG(I, 2)
    Il = threshold_rel(Il, 0.1)

    Il = restore_size(Il, Iraw)

    slices = list(slices_points.keys())
    slices.sort()

    plt.gray()
    fig, ax = plt.subplots(1)
    plt.ion()
    for i in slices:
        plt.title("Frame #" + str(i))
        ax.imshow(Il[:, :, i-1])
        for point in slices_points[i]:
            circle = patches.Circle((point[0], point[1]), 1)
            circle.set_color('r')
            ax.add_patch(circle)
        plt.show()
        plt.waitforbuttonpress()
        ax.clear()